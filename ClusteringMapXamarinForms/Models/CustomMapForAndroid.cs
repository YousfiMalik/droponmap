﻿using System;
using System.Collections.Generic;
using ClusteringMapXamarinForms;

namespace ClusteringMapsXamarinForms
{
	public class CustomMapForAndroid : Xamarin.Forms.Maps.Map
	{
		public CustomMapForAndroid()
		{
			
		}
		public List<CustomPinForAndroid> CustomPins { get; set; }

		private Parameter _parameter;
		public Parameter Parameter
		{
			get { return _parameter; }
			set
			{
				_parameter = value;
				OnPropertyChanged();
			}
		}
	}
	public enum Parameter
	{
		test,
	}
}
