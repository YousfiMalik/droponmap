﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using ClusteringMapXamarinForms.CustomFormElements;
using Xamarin.Forms;

namespace ClusteringMapXamarinForms
{
	public partial class Details : ModalPage
	{

        ICommand CloseCommand { get; }

		public Details()
		{
			InitializeComponent();
			CloseCommand = new Command(() => Close());

			Shadow1.GestureRecognizers.Add(new TapGestureRecognizer { Command = CloseCommand });
			Shadow2.GestureRecognizers.Add(new TapGestureRecognizer { Command = CloseCommand });
			Shadow3.GestureRecognizers.Add(new TapGestureRecognizer { Command = CloseCommand });
			Shadow4.GestureRecognizers.Add(new TapGestureRecognizer { Command = CloseCommand });

		}
		public void setImage(String imgPath)
		{
			this.myImage.Source=imgPath;
		}
	}
}
