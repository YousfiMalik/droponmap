﻿using System;
namespace ClusteringMapXamarinForms
{
	public class Drop
	{
		public int id;
		public string type;
		public float latitude;
		public float longitude;
		public float altitude;
		public string thumbnail;
		public string emoji;
		public DateTime registered;
		public int follow;
		public int eventspecific;
		public int venuespecific;

		public Drop()
		{
		}
	}
}
