﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms.GoogleMaps;
namespace ClusteringMapXamarinForms.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();
			Xamarin.FormsGoogleMaps.Init("AIzaSyCGvFtCPUbkcjACw-yh5_AcJOj9OcMmfgU");
			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}
